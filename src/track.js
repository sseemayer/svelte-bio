import { Rect, Point, View } from './layout'

export class TrackDefinition {
	constructor (caption, height, options) {
		this.caption = caption
		this.height = height

		this.buffer = null
		this.options = Object.assign({
			alwaysRedraw: false,
			bufferOffset: false,
			bufferDestination: false,
			marginRight: 0,
		}, options)
	}

	makeBuffer ({ width, height }) {
		if (this.buffer) { return; }
		const canvas = document.createElement('canvas')
		canvas.width = width
		canvas.height = height

		const context = canvas.getContext('2d')
		this.buffer = {
			canvas,
			context,
		}
	}

	onPaint ({ ctx, view, multiTrack }) {
		// if the track has an onPaintBuffer function, this indicates
		// that the buffer shall be painted once and further painting
		// will be done by blitting that buffer.
		if (this.onPaintBuffer) {
			let shouldDraw = !!this.options.alwaysRedraw

			if (!this.buffer) {
				this.makeBuffer({ width: multiTrack.width, height: this.height })
				shouldDraw = true
			}

			if (shouldDraw) {
				const bufferCtx = this.buffer.context
				let bufferDestination = new Rect(0, 0, multiTrack.width, this.height)
				if (this.options.bufferDestination) {
					bufferDestination.width = view.destination.width
					bufferDestination.height = view.destination.height
				}
				let bufferInnerOffset = new Point(0, 0)
				if (this.options.bufferOffset) {
					bufferInnerOffset = view.innerOffset
				}

				const bufferView = new View(bufferDestination, bufferInnerOffset)

				this.onPaintBuffer({ ctx: bufferCtx, view: bufferView, multiTrack })
			}
		}

		if (this.buffer) {
			const dst = view.destination
			const ofs = view.innerOffset
			ctx.drawImage(
				this.buffer.canvas,
				ofs.x,
				ofs.y,
				dst.width,
				dst.height - ofs.y,
				dst.left,
				dst.top + ofs.y,
				dst.width,
				dst.height - ofs.y,
			)
		}
	}

}
