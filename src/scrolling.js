export class Scrollbar {

	// 
	// /--------size---------\
	// [   XXXXX             ]
	// |   |   |             |
	// min |   |           max
	//     |___|
	//     window
	//     |
	//     value

	constructor (max, min, value, window, scrollSpeed) {
		this.max = max
		this.min = min
		this.value = value
		this.window = window

		this.savePos = null
		this.saveValue = null
		this.scrollSpeed = scrollSpeed || 1
	}

	render (size) {
		const range = this.max - this.min

		return {
			size: Math.round((this.window / range) * size),
			offset: Math.round(this.value / range * size),
		}
	}
	
	onMouseDown (size, pos) {
		const dims = this.render(size)

		if (pos >= dims.offset && pos < dims.offset + dims.size) {
			// clicked on scrollbar
			this.savePos = pos
			this.saveValue = this.value
		} else if (pos < dims.offset){
			// clicked before scrollbar
		} else {
			// clicked after scrollbar
		}
	}

	onMouseMove (size, pos) {
		if (!this.savePos) { return }
		
		const range = this.max - this.min

		// pixel-level offset
		const offset = pos - this.savePos

		let newValue = this.saveValue + offset * range / size
		newValue = Math.round(newValue)

		if (newValue < this.min) {
			newValue = this.min
		}

		if (newValue > this.max - this.window) {
			newValue = this.max - this.window
		}


		this.value = newValue
	}

	onMouseUp () {
		this.savePos = null
		this.saveValue = null
	}

	onScroll (delta) {
		let newValue = this.value + delta * this.scrollSpeed
		newValue = Math.round(newValue)

		if (newValue < this.min) {
			newValue = this.min
		}

		if (newValue > this.max - this.window) {
			newValue = this.max - this.window
		}

		this.value = newValue
		
	}

}
