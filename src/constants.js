export const aminoAcids = ['A', 'C', 'D', 'E', 'F', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']
export const nucleotides = ['A', 'C', 'G', 'T', 'U']

export const colors = {

	weblogoChemistry: {
		'GSTYC': '#006600',
		'QN': '#660066',
		'KRH': '#000066',
		'DE': '#660000',
		'AVLIPWFM': '#000000',
	}

}
