export class Rect {
	constructor ( left, top, width, height ) {
		this.left = left
		this.top = top
		this.width = width
		this.height = height
	}
}

export class Point {
	constructor ( x, y ) {
		this.x = x
		this.y = y
	}
}

export class View {
	constructor ( destination, innerOffset ) {
		this.destination = destination
		this.innerOffset = innerOffset
	}

	visibleIn (outerView) {
		const id = this.destination
		const od = outerView.destination

		return (id.top + id.height >= od.top) &&
			(id.top <= od.top + od.height) &&
			(id.left + id.width >= od.left) &&
			(id.left <= od.left + od.width)
	}

	offsetDestination ( offset ) {
		let newDestination = new Rect(
			this.destination.left + offset.left,
			this.destination.top + offset.top,
			this.destination.width + offset.width,
			this.destination.height + offset.height,
		)

		let newInnerOffset = new Point(
			this.innerOffset.x,
			this.innerOffset.y,
		)

		return new View(newDestination, newInnerOffset)
	}
}
