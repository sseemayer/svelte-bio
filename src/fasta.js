
const header = /^\>(\S+)\s*(.*)/

export function parseFASTA (content) {
	let out = []
	let current = null
	for (let line of content.split('\n')) {
		line = line.trim()

		const headerMatch = line.match(header)
		if (headerMatch) {
			if (current !== null) {
				out.push(current)
			}

			current = {
				identifier: headerMatch[1],
				description: headerMatch[2],
				sequence: '',
			}
		} else {
			current.sequence += line
		}
	}

	if (current !== null) {
		out.push(current)
	}

	return out
}
