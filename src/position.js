export class Position {
	constructor (i) {
		this.i = i
	}
}


export function fromSequence (sequence) {
	return sequence.split('').map((_, i) => new Position(i + 1))
}
