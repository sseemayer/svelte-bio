import opentype from 'opentype.js'

import { createFilter } from '@rollup/pluginutils'

const defaultInclude = ['**/*.ttf', '**/*.otf']
const defaultGlyphSet = ['A', 'C', 'D', 'E', 'F', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']

export default function glyph (options = {}) {
	const { 
		include = defaultInclude,
		glyphSet = defaultGlyphSet,
		exclude,
	} = options

	const filter = createFilter(include, exclude)

	return {
		name: 'glyph',
		load: async function load (id) {
			if (!filter(id)) { return null }
			console.log(id)

			const font = await opentype.load(id)

			const glyphs = Object.fromEntries(glyphSet.map((g) => {
				const glyph = font.charToGlyph(g)
				const path = glyph.getPath()

				return [g, {
					xMin: glyph.xMin,
					yMin: glyph.yMin,
					xMax: glyph.xMax,
					yMax: glyph.yMax,
					bbox: path.getBoundingBox(),
					path: path.toPathData(),
				}]}

			))

			const data = JSON.stringify(glyphs)

			return `export default ${data}`
		},
	}
}
